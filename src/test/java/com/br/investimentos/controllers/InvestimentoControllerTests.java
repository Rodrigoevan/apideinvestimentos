package com.br.investimentos.controllers;


import com.br.investimentos.enums.RiscoDoInvestimento;
import com.br.investimentos.models.Investimento;
import com.br.investimentos.security.JWTUtil;
import com.br.investimentos.services.InvestimentoService;
import com.br.investimentos.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(InvestimentoController.class)
@Import(JWTUtil.class)
public class InvestimentoControllerTests {

    public static String INVESTIMENTO_NAO_ENCONTRADO = "Investimento não encontrado";

    @MockBean
    private InvestimentoService investimentoService;

    @MockBean
    private UsuarioService usuarioService;
    @MockBean
    private JWTUtil jwtUtil;


    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    private Investimento investimento = new Investimento();
    private String username = "rodrigo@gmail.com";
    private String password = "aviao11";
    private String token;

    @BeforeEach
    public void iniciar() throws Exception {
        investimento.setId(1);
        investimento.setPorcentagemLucro(0.05);
        investimento.setRiscoDoInvestimento(RiscoDoInvestimento.BAIXO);
        investimento.setNome("Poupança");
        investimento.setDescricao("Poupança rende 0,05 a.m");

    }
    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarBuscarTodosInvestimentos() throws Exception {
        Iterable<Investimento> investimentosIterable = Arrays.asList(investimento);
        Mockito.when(investimentoService.buscarTodosInvestimentos()).thenReturn(investimentosIterable);

        String json = mapper.writeValueAsString(investimentosIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/todosInvestimentos"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));

    }
    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarBuscarInvestimentoOK() throws Exception {
        Mockito.when(investimentoService.buscarPorId(Mockito.anyInt()))
                .thenReturn(Optional.ofNullable(investimento));

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/1"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.id", CoreMatchers.equalTo(1)));

    }
    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarBuscarInvestimentoExcecao() throws Exception {
        Mockito.when(investimentoService.buscarPorId(Mockito.anyInt()))
                .thenReturn(Optional.empty());

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/2"))
                .andExpect(status().isBadRequest())
                .andExpect(status().reason(containsString("Id não existente")));

    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarInserirInvestimento() throws Exception {
        Iterable<Investimento> investimentoIterable = Arrays.asList(investimento);
        Mockito.when(investimentoService.salvarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos/salvar")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers
                        .status().isCreated())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.id", CoreMatchers.equalTo(1)));

    }
    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarAtualizarInvestimento() throws Exception {
        Mockito.when(investimentoService.atualizarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers
                        .status().isOk())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.id", CoreMatchers.equalTo(1)));

    }
    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarAtualizarInvestimentoInexistente() throws Exception {
        Mockito.when(investimentoService.atualizarInvestimento(Mockito.any(Investimento.class)))
                .thenThrow(new ObjectNotFoundException(Investimento.class, INVESTIMENTO_NAO_ENCONTRADO));

        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.put("/investimentos/4")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers
                        .status().isBadRequest())
                .andExpect(status().reason(INVESTIMENTO_NAO_ENCONTRADO));
        ;

    }

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarDeletarInvestimento() throws Exception {
        Mockito.when(investimentoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.ofNullable(investimento));
        Mockito.when(investimentoService.deletarInvestimento(Mockito.any(Investimento.class)))
                .thenReturn(investimento);
        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.delete("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status().isOk());

    }
    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarDeletarInvestimentoInexistente() throws Exception {
        Mockito.when(investimentoService.buscarPorId(Mockito.anyInt())).thenReturn(Optional.empty());
        String json = mapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.delete("/investimentos/5")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers
                        .status().isNoContent());

    }



}

