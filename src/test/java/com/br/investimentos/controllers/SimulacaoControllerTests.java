package com.br.investimentos.controllers;


import com.br.investimentos.models.RetornoInvestimento;
import com.br.investimentos.models.Simulacao;
import com.br.investimentos.security.JWTUtil;
import com.br.investimentos.services.SimulacaoService;
import com.br.investimentos.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(SimulacaoController.class)
@Import(JWTUtil.class)
public class SimulacaoControllerTests {

    private static final String INVESTIMENTO_NAO_ENCONTRADO = "Investimento  Não encontrado";

    @MockBean
    SimulacaoService simulacaoService;
    @MockBean
    UsuarioService usuarioService;

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    private Simulacao simulacao = new Simulacao(1,1,100.0);

    private RetornoInvestimento retornoInvestimento = new RetornoInvestimento(105.0);

    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarCalcularInvestimento () throws Exception{
        Mockito.when(simulacaoService.retornarSimulacaoInvestimento(Mockito.any())).thenReturn(retornoInvestimento);

        String json = mapper.writeValueAsString(simulacao);

        mockMvc.perform(MockMvcRequestBuilders.put("/simulacao")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers
                        .status().isOk())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.retornoInvestimento",CoreMatchers.equalTo(105.0)));

    }
    @Test
    @WithMockUser(username = "usuario@gmail.com", password = "aviao11")
    public void testarCalcularInvestimentoInexistente () throws Exception{
        Mockito.when(simulacaoService.retornarSimulacaoInvestimento(Mockito.any()))
                .thenThrow(new ObjectNotFoundException(Simulacao.class,INVESTIMENTO_NAO_ENCONTRADO));

        String json = mapper.writeValueAsString(simulacao);

        mockMvc.perform(MockMvcRequestBuilders.put("/simulacao")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers
                        .status().isBadRequest())
                .andExpect(status().reason(INVESTIMENTO_NAO_ENCONTRADO));

        ;

    }

}
