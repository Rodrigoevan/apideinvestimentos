package com.br.investimentos.services;

import com.br.investimentos.enums.RiscoDoInvestimento;
import com.br.investimentos.models.Investimento;
import com.br.investimentos.models.RetornoInvestimento;
import com.br.investimentos.models.Simulacao;
import com.br.investimentos.repositories.InvestimentoRepository;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class SimulacaoServiceTests {


    @MockBean
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;

    private Investimento investimento = new Investimento();

    private Simulacao simulacao = new Simulacao();

    private RetornoInvestimento retornoInvestimento =new  RetornoInvestimento();
    @BeforeEach
    void iniciar(){
        simulacao.setDinheiroAplicado(100.0);
        simulacao.setInvestimentoId(1);
        simulacao.setMesesDeAplicacao(1);

        investimento.setId(1);
        investimento.setPorcentagemLucro(0.05);
        investimento.setRiscoDoInvestimento(RiscoDoInvestimento.BAIXO);
        investimento.setNome("Poupança");
        investimento.setDescricao("Poupança rende 0,05 a.m");


    }

    @Test
    public void testarRetornarSimulacaoInvestimentoCorreto() throws Exception {
        retornoInvestimento.setRetornoInvestimento(105.0);
        Mockito.when(investimentoRepository.findById(investimento.getId())).thenReturn(Optional.of(investimento));
        RetornoInvestimento retornoInvestimentoObjeto = simulacaoService.retornarSimulacaoInvestimento(simulacao);
        Mockito.verify(investimentoRepository).findById(investimento.getId());
        Assertions.assertEquals(retornoInvestimentoObjeto.getRetornoInvestimento(),
                                retornoInvestimento.getRetornoInvestimento());

    }
    @Test
    public void testarRetornarSimulacaoInvestimentoIncorreto() throws Exception {
        retornoInvestimento.setRetornoInvestimento(102.0);
        Mockito.when(investimentoRepository.findById(investimento.getId())).thenReturn(Optional.of(investimento));
        RetornoInvestimento retornoInvestimentoObjeto = simulacaoService.retornarSimulacaoInvestimento(simulacao);
        Mockito.verify(investimentoRepository).findById(investimento.getId());
        Assertions.assertNotEquals(retornoInvestimentoObjeto,retornoInvestimento.getRetornoInvestimento());

    }
    @Test
    public void testarRetornarSimulacaoInvestimentoInvestimentoIdInexistente() throws Exception {
        retornoInvestimento.setRetornoInvestimento(102.0);
        Mockito.when(investimentoRepository.findById(investimento.getId())).thenReturn(Optional.empty());
//        RetornoInvestimento retornoInvestimentoObjeto = simulacaoService.retornarSimulacaoInvestimento(simulacao);
        Assertions.assertThrows(ObjectNotFoundException.class, () -> simulacaoService.retornarSimulacaoInvestimento(simulacao),
                "Investimento  Não encontrado");
        Mockito.verify(investimentoRepository).findById(investimento.getId());

    }

}
