package com.br.investimentos.services;

import com.br.investimentos.enums.RiscoDoInvestimento;
import com.br.investimentos.models.Investimento;
import com.br.investimentos.repositories.InvestimentoRepository;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class InvestimentoServiceTests {

    public static String INVESTIMENTO_NAO_ENCONTRADO = "Investimento não encontrado";
    @MockBean
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private InvestimentoService investimentoService;

    private Investimento investimento = new Investimento();
    private Investimento investimentoAlterado = new Investimento("Poupança Nova",
            "poupanca Nova rende 0,10 a.m",RiscoDoInvestimento.BAIXO,0.10);
    private List<Investimento> investimentos = new ArrayList<>();

    @BeforeEach
    public void iniciar(){
        investimento.setId(1);
        investimento.setPorcentagemLucro(0.05);
        investimento.setRiscoDoInvestimento(RiscoDoInvestimento.BAIXO);
        investimento.setNome("Poupança");
        investimento.setDescricao("Poupança rende 0,05 a.m");

        investimentos.add(investimento);

    }
    @Test
    public void testarBuscarTodosInvestimentos() throws Exception {
        Iterable<Investimento> investimentosIterable = Arrays.asList(investimento);
        Mockito.when(investimentoRepository.findAll()).thenReturn(investimentosIterable);
        Iterable<Investimento> investimentosObjeto = investimentoService.buscarTodosInvestimentos();
        Mockito.verify(investimentoRepository).findAll();
        Assertions.assertEquals(investimentosObjeto, investimentosIterable);
    }
    @Test
    public void testarBuscarPorId() throws Exception {
        Mockito.when(investimentoRepository.findById(investimento.getId())).thenReturn(Optional.of(investimento));
        Optional<Investimento> investimentoObjeto = investimentoService.buscarPorId(investimento.getId());
        Mockito.verify(investimentoRepository).findById(investimento.getId());
        Assertions.assertEquals(investimentoObjeto, Optional.of(investimento));
    }

    @Test
    public void testarBuscarPorIdinvalido() throws Exception {
        Mockito.when(investimentoRepository.findById(investimento.getId())).thenReturn(Optional.empty());
        Optional<Investimento> investimentoObjeto = investimentoService.buscarPorId(3);
        Assertions.assertEquals(investimentoObjeto, Optional.empty());
    }
    @Test
    public void testarSalvarInvestimento() throws Exception {
        Mockito.when(investimentoRepository.save(investimento)).thenReturn(investimento);
        Investimento investimentoSalvo = investimentoService.salvarInvestimento(investimento);
        Mockito.verify(investimentoRepository).save(investimento);
        Assertions.assertEquals(investimentoSalvo, investimento);
    }


    @Test
    public void testarAtualizarInvestimento() throws Exception {
        investimentoAlterado.setId(1);
        investimentoAlterado.setPorcentagemLucro(0.10);
        investimentoAlterado.setRiscoDoInvestimento(RiscoDoInvestimento.BAIXO);
        investimentoAlterado.setNome("Poupança Nova");
        investimentoAlterado.setDescricao("Poupança rende 0,10 a.m");

        Mockito.when(investimentoRepository.findById(investimento.getId()))
                .thenReturn(Optional.of(investimento));
        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class)))
                .thenReturn(investimentoAlterado);
        Investimento investimentoObjeto = investimentoService.atualizarInvestimento(investimentoAlterado);
        Mockito.verify(investimentoRepository).save(investimentoAlterado);
        Assertions.assertEquals(investimentoObjeto, investimentoAlterado);

    }

    @Test
    public void testarAtualizarInvestimentoInexistente() throws Exception {
        investimentoAlterado.setId(2);
        investimentoAlterado.setPorcentagemLucro(0.10);
        investimentoAlterado.setRiscoDoInvestimento(RiscoDoInvestimento.BAIXO);
        investimentoAlterado.setNome("Poupança Nova");
        investimentoAlterado.setDescricao("Poupança rende 0,10 a.m");

        Mockito.when(investimentoRepository.findById(investimentoAlterado.getId()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(ObjectNotFoundException.class,
                () -> investimentoService.atualizarInvestimento(investimentoAlterado),
                INVESTIMENTO_NAO_ENCONTRADO);
        Mockito.verify(investimentoRepository).findById(investimentoAlterado.getId());

    }


    @Test
    public void testarDeletarInvestimento() throws Exception {
        Mockito.when(investimentoRepository.findById(investimento.getId()))
                .thenReturn(Optional.of(investimento));
        investimentoService.deletarInvestimento(investimento);
        Mockito.verify(investimentoRepository).delete(Mockito.any(Investimento.class));

    }
    @Test
    public void testarDeletarInvestimentoInexistente() throws Exception {
        Mockito.when(investimentoRepository.findById(investimento.getId()))
                .thenReturn(Optional.empty());
        Assertions.assertThrows(ObjectNotFoundException.class,
                () -> investimentoService.deletarInvestimento(investimentoAlterado),
                INVESTIMENTO_NAO_ENCONTRADO);


    }

}
