package com.br.investimentos.services;

import com.br.investimentos.models.Investimento;
import com.br.investimentos.models.RetornoInvestimento;
import com.br.investimentos.models.Simulacao;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SimulacaoService {

    private static final String INVESTIMENTO_NAO_ENCONTRADO = "Investimento  Não encontrado";
    @Autowired
    private RetornoInvestimento retornoInvestimento;

    @Autowired
    private InvestimentoService investimentoService;


    public RetornoInvestimento retornarSimulacaoInvestimento(Simulacao simulacao) throws ObjectNotFoundException {
            Optional<Investimento> investimento = investimentoService.buscarPorId(simulacao.getInvestimentoId());
            if(investimento.isPresent()){
                Double valorRetornoInvestimento = 0.0;
                    valorRetornoInvestimento = simulacao.getDinheiroAplicado() *
                                               Math.pow((1+investimento.get().getPorcentagemLucro()),
                                                       simulacao.getMesesDeAplicacao());
                retornoInvestimento.setRetornoInvestimento(valorRetornoInvestimento);
                return retornoInvestimento;
            }
            throw new ObjectNotFoundException(Investimento.class, INVESTIMENTO_NAO_ENCONTRADO);

    }

}
