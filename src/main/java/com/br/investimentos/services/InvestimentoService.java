package com.br.investimentos.services;

import com.br.investimentos.models.Investimento;
import com.br.investimentos.repositories.InvestimentoRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InvestimentoService {

    public static String INVESTIMENTO_NAO_ENCONTRADO = "Investimento não encontrado";
    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Iterable<Investimento> buscarTodosInvestimentos() {
        Iterable<Investimento> investimentos = investimentoRepository.findAll();
        return investimentos;

    }

    public Optional<Investimento> buscarPorId(Integer id) {
        Optional<Investimento> investimentoOptional = investimentoRepository.findById(id);
        return investimentoOptional;
    }

    public Investimento salvarInvestimento(Investimento investimento) {

        Investimento investimentoObjeto = investimentoRepository.save(investimento);
        return investimentoObjeto;
    }

    public Investimento atualizarInvestimento(Investimento investimento) throws ObjectNotFoundException {
        Optional<Investimento> investimentoOptional = buscarPorId(investimento.getId());
        if (investimentoOptional.isPresent()){
            Investimento investimentoObjeto =investimentoRepository.save(investimento);
            return investimentoObjeto;
        }
        throw new ObjectNotFoundException(Investimento.class, INVESTIMENTO_NAO_ENCONTRADO);
    }


    public Investimento deletarInvestimento(Investimento investimento) {
        Optional<Investimento> investimentoOptional = buscarPorId(investimento.getId());
        if (investimentoOptional.isPresent()){
            investimentoRepository.delete(investimento);
            return investimento;
        }
        throw new ObjectNotFoundException(Investimento.class, INVESTIMENTO_NAO_ENCONTRADO);

    }
}
