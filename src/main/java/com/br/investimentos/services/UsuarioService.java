package com.br.investimentos.services;

import com.br.investimentos.models.Usuario;
import com.br.investimentos.repositories.UsuarioRepository;
import com.br.investimentos.security.DetalhesUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UsuarioService implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public Usuario salvarUsuario(Usuario usuario){
        Usuario userOBJ = usuarioRepository.findByEmail(usuario.getEmail());

        if(userOBJ != null){
            throw new RuntimeException("Esse email já existe");
        }

        String senha = usuario.getSenha();
        String encode = bCryptPasswordEncoder.encode(senha);
        usuario.setSenha(encode);
        return usuarioRepository.save(usuario);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Usuario usuario = usuarioRepository.findByEmail(email);
        if(usuario == null){
            throw new UsernameNotFoundException(email);
        }
        DetalhesUsuario usuarioDetails = new DetalhesUsuario(usuario.getId(), usuario.getEmail(), usuario.getSenha());
        return usuarioDetails;
    }

}
