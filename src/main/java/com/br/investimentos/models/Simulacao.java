package com.br.investimentos.models;


import com.sun.istack.NotNull;
import org.springframework.boot.jackson.JsonComponent;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;

@JsonComponent
public class Simulacao {

    @NotNull
    private Integer investimentoId;

    @NotNull
    @DecimalMin("1")
    private Integer mesesDeAplicacao;

    @NotNull
    @DecimalMin("100.0")
    private Double dinheiroAplicado;

    public Simulacao() {
    }

    public Simulacao(Integer investimentoId, Integer mesesDeAplicacao, Double dinheiroAplicado) {
        this.investimentoId = investimentoId;
        this.mesesDeAplicacao = mesesDeAplicacao;
        this.dinheiroAplicado = dinheiroAplicado;
    }

    public Integer getInvestimentoId() {
        return investimentoId;
    }

    public void setInvestimentoId(Integer investimentoId) {
        this.investimentoId = investimentoId;
    }

    public Integer getMesesDeAplicacao() {
        return mesesDeAplicacao;
    }

    public void setMesesDeAplicacao(Integer mesesDeAplicacao) {
        this.mesesDeAplicacao = mesesDeAplicacao;
    }

    public Double getDinheiroAplicado() {
        return dinheiroAplicado;
    }

    public void setDinheiroAplicado(Double dinheiroAplicado) {
        this.dinheiroAplicado = dinheiroAplicado;
    }
}
