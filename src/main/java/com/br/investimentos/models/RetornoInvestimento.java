package com.br.investimentos.models;

import org.springframework.boot.jackson.JsonComponent;

@JsonComponent
public class RetornoInvestimento {

    private Double retornoInvestimento;

    public RetornoInvestimento() {
    }

    public RetornoInvestimento(Double retornoInvestimento) {
        this.retornoInvestimento = retornoInvestimento;
    }

    public Double getRetornoInvestimento() {
        return retornoInvestimento;
    }

    public void setRetornoInvestimento(Double retornoInvestimento) {
        this.retornoInvestimento = retornoInvestimento;
    }
}
