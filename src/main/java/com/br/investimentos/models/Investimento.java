package com.br.investimentos.models;

import com.br.investimentos.enums.RiscoDoInvestimento;
import net.bytebuddy.implementation.bind.annotation.Empty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @NotBlank
    @NotEmpty
    private String nome;
    @NotNull
    private String descricao;
    @NotNull
    private RiscoDoInvestimento riscoDoInvestimento;
    @NotNull
    @DecimalMin("0.01")
    private Double porcentagemLucro;

    public Investimento() {
    }

    public Investimento(String nome, String descricao, RiscoDoInvestimento riscoDoInvestimento, Double porcentagemLucro) {
        this.nome = nome;
        this.descricao = descricao;
        this.riscoDoInvestimento = riscoDoInvestimento;
        this.porcentagemLucro = porcentagemLucro;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public RiscoDoInvestimento getRiscoDoInvestimento() {
        return riscoDoInvestimento;
    }

    public void setRiscoDoInvestimento(RiscoDoInvestimento riscoDoInvestimento) {
        this.riscoDoInvestimento = riscoDoInvestimento;
    }

    public Double getPorcentagemLucro() {
        return porcentagemLucro;
    }

    public void setPorcentagemLucro(Double porcentagemLucro) {
        this.porcentagemLucro = porcentagemLucro;
    }

    public int getId() {return id; }

    public void setId(int id) { this.id = id; }

}
