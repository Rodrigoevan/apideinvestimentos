package com.br.investimentos.controllers;

import com.br.investimentos.models.RetornoInvestimento;
import com.br.investimentos.models.Simulacao;
import com.br.investimentos.services.SimulacaoService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;


@RestController
@RequestMapping("/simulacao")
public class SimulacaoController {

    private static final String INVESTIMENTO_NAO_ENCONTRADO = "Investimento  Não encontrado";
    @Autowired
    private RetornoInvestimento retornoInvestimento;

    @Autowired
    private SimulacaoService simulacaoService;

    @PutMapping
    public RetornoInvestimento calcularInvestimento(@RequestBody @Valid Simulacao simulacao) throws ResponseStatusException {
        try {
            retornoInvestimento = simulacaoService.retornarSimulacaoInvestimento(simulacao);
            return retornoInvestimento;
        }
        catch (ObjectNotFoundException objectNotFoundException){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, INVESTIMENTO_NAO_ENCONTRADO);
        }
    }


}
