package com.br.investimentos.controllers;

import com.br.investimentos.models.Investimento;
import com.br.investimentos.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    private static final String INVESTIMENTO_NAO_ENCONTRADO = "Investimento não encontrado";;
    @Autowired
    private InvestimentoService investimentoService;

    @GetMapping("/{id}")
    public Investimento buscarInvestimento(@PathVariable @Valid Integer id) throws Exception{
        Optional<Investimento> investimentoOptional = investimentoService.buscarPorId(id);
        if (investimentoOptional.isPresent()){
            return investimentoOptional.get();
        }else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Id não existente");
        }

    }
    @GetMapping("/todosInvestimentos")
    public Iterable<Investimento> buscarTodosInvestimentos(){
        return investimentoService.buscarTodosInvestimentos();
    }


    @PostMapping("/salvar")
    public ResponseEntity<Investimento> salvarInvestimento(@RequestBody @Valid Investimento investimento){
        investimentoService.salvarInvestimento(investimento);
        return ResponseEntity.status(201).body(investimento);
    }

    @PutMapping("/{id}")
    public Investimento atualizarInvestimento(@PathVariable Integer id,
                                              @RequestBody @Valid Investimento investimento) throws ResponseStatusException {

        investimento.setId(id);
        try{
            Investimento investimentoObjeto = investimentoService.atualizarInvestimento(investimento);
            return investimentoObjeto;
        }
        catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,INVESTIMENTO_NAO_ENCONTRADO);
        }

    }

    @DeleteMapping("/{id}")
    public Investimento deletarInvestimento(@PathVariable Integer id) throws ResponseStatusException {
        Optional<Investimento> investimentoOptional = investimentoService.buscarPorId(id);
        if (investimentoOptional.isPresent()){
            Investimento investimentoObjeto = investimentoService.deletarInvestimento(investimentoOptional.get());
            return investimentoObjeto;
        }
        throw new ResponseStatusException(HttpStatus.NO_CONTENT);
    }


}
