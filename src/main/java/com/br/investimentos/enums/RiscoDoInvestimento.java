package com.br.investimentos.enums;

public enum RiscoDoInvestimento {
    ALTO,
    MEDIO,
    BAIXO;
}
